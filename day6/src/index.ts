import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, '../dist/sample-input.txt')
let challengePath = path.join(__dirname, '../dist/input.txt')
let challengePath2 = path.join(__dirname, '../dist/input2.txt')

const challengeCode = (data: String) => {
  let lanternfish = parseData(data)
  let lanternfishRewrite = parseDataRewrite(data)
  console.log('Part 1')
  console.log(estimateLanternFishRewrite(80, new Map(lanternfishRewrite)))
  console.log('Part 2')
  console.log(estimateLanternFishRewrite(256, new Map(lanternfishRewrite)))
}

const estimateLanternfish = (days: number, lanternfish: Array<number>): number => {
  let day = 0
  let tempLanternfish = lanternfish.slice()
  while (day < days) {
    tempLanternfish.forEach((lf, i) => {
      if (lf === 0) {
        lanternfish[i] = 6
        lanternfish.push(8)
      } else {
        lanternfish[i]--
      }
    })
    tempLanternfish = lanternfish.slice()
    day++
  }
  return lanternfish.length
}

const estimateLanternFishRewrite = (days: number, lanternfish: Map<number, number>): number => {
  let day = 0
  while (day < days) {
    let readyFish = 0
    Array.from(lanternfish.keys()).sort().forEach(key => {
      if (key === 0) {
        readyFish = lanternfish.get(key) || 0
      } else {
        lanternfish.set(key-1, lanternfish.get(key) || 0)
        lanternfish.set(key, 0)
      }
    })
    lanternfish.set(6, (lanternfish.get(6) || 0) + readyFish)
    lanternfish.set(8, readyFish)
    day++
  }
  return Array.from(lanternfish.values()).reduce((prev, curr) => prev + curr, 0)
}

const parseData = (data: String): Array<number> => data.split(',').map(x => parseInt(x))

const parseDataRewrite = (data: String): Map<number, number> => data.split(',')
  .map(x => parseInt(x))
  .reduce((prev, curr) => {
    let currVal = prev.get(curr) || 0
    prev.set(curr, currVal + 1)
    return prev
  }, new Map<number, number>())

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}