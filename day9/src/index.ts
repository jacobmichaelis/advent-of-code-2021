import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, '../dist/sample-input.txt')
let challengePath = path.join(__dirname, '../dist/input.txt')
let challengePath2 = path.join(__dirname, '../dist/input2.txt')

class Point {
  x: number
  y: number
  val: number
  constructor(x: number, y: number, val: number) {
    this.x = x
    this.y = y
    this.val = val
  }
}

const challengeCode = (data: String) => {
  let heightMap = parseData(data)
  let lowPoints = getLowPoints(heightMap)
  console.log(`Part 1: ${lowPoints.reduce((a, b) => a + b.val + 1, 0)}`)
  let basins: Array<Array<Point>> = lowPoints.map(p => findBasin(heightMap, p, new Array<Point>()))
  let basinsBySize = basins.sort((a, b) => b.length - a.length)
  let basinProduct = basinsBySize[0].length * basinsBySize[1].length * basinsBySize[2].length
  console.log(`Part 2: ${basinProduct}`)
}

const getLowPoints = (heightMap: Array<Array<number>>): Array<Point> => {
  let lowPoints = new Array<Point>()
  heightMap.forEach((row, i) => {
    row.forEach((num, j) => {
      let up = i > 0 ? num < heightMap[i - 1][j] : true
      let down = i < heightMap.length - 1 ? num < heightMap[i + 1][j] : true
      let left = j > 0 ? num < row[j - 1] : true
      let right = j < row.length - 1 ? num < row[j + 1] : true
      if (up && down && left && right) lowPoints.push(new Point(i, j, num))
    })
  })
  return lowPoints
}

const findBasin = (heightMap: Array<Array<number>>, point: Point, visited: Array<Point>): Array<Point> => {
  visited.push(point)
  // traversal checks
  let height = heightMap.length
  let width = heightMap[0].length
  if (point.x > 0 && heightMap[point.x - 1][point.y] < 9) {
    let targetPoint = new Point(point.x - 1, point.y, heightMap[point.x - 1][point.y])
    if (notVisited(visited, targetPoint)) findBasin(heightMap, targetPoint, visited)
  }
  if (point.x < height - 1 && heightMap[point.x + 1][point.y] < 9) {
    let targetPoint = new Point(point.x + 1, point.y, heightMap[point.x + 1][point.y])
    if (notVisited(visited, targetPoint)) findBasin(heightMap, targetPoint, visited)
  }
  if (point.y > 0 && heightMap[point.x][point.y - 1] < 9) {
    let targetPoint = new Point(point.x, point.y - 1, heightMap[point.x][point.y - 1])
    if (notVisited(visited, targetPoint)) findBasin(heightMap, targetPoint, visited)
  }
  if (point.y < width - 1 && heightMap[point.x][point.y + 1] < 9) {
    let targetPoint = new Point(point.x, point.y + 1, heightMap[point.x][point.y + 1])
    if (notVisited(visited, targetPoint)) findBasin(heightMap, targetPoint, visited)
  }
  return visited
}

const notVisited = (visited: Array<Point>, point: Point): boolean => {
  return !visited.find(p => p.x === point.x && p.y === point.y)
}

const parseData = (data: String): Array<Array<number>> => {
  return data.split('\n').map(x => Array.from(x).map(y => parseInt(y)))
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}