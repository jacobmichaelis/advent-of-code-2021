import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, "../dist/sample-input.txt")
let challengePath = path.join(__dirname, "../dist/input.txt")
let challengePath2 = path.join(__dirname, "../dist/input2.txt")

class Direction {
  dir: string
  dist: number
  constructor(dir: string, dist: number) {
    this.dir = dir
    this.dist = dist
  }
}

const challengeCode = (data: string) => {
  let input = parseInput(data)
  console.log("Solution 1")
  console.log(findPositionProduct(input))
  console.log("Solution 2")
  console.log(findPositionWithAim(input))
}

const parseInput = (input: string): Array<Direction> => {
  return input.split('\n').map(x => {
    let kvPair = x.split(' ')
    return new Direction(kvPair[0], parseInt(kvPair[1]))
  })
}

const findPositionProduct = (dirs: Array<Direction>): number => {
  let hor = 0
  let ver = 0
  dirs.forEach(dir => {
    switch(dir.dir) {
      case 'forward': hor += dir.dist; break;
      case 'down': ver += dir.dist; break;
      case 'up': ver -= dir.dist; break;
    }
  })
  return hor * ver
}

const findPositionWithAim = (dirs: Array<Direction>): number => {
  let hor = 0
  let depth = 0
  let aim = 0
  dirs.forEach(dir => {
    switch(dir.dir) {
      case 'forward': hor += dir.dist; depth += aim * dir.dist; break;
      case 'down': aim += dir.dist; break;
      case 'up': aim -= dir.dist; break;
    }
  })
  return hor * depth
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}