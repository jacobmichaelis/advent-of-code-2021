import * as fs from 'fs'
import * as path from 'path'
import { stringify } from 'querystring'

let samplePath = path.join(__dirname, "../dist/sample-input.txt")
let challengePath = path.join(__dirname, "../dist/input.txt")
let challengePath2 = path.join(__dirname, "../dist/input2.txt")

class Point {
  x: number
  y: number
  constructor (x: number, y: number) {
    this.x = x
    this.y = y
  }
}

class Line {
  p1: Point
  p2: Point
  constructor (x1: number, y1: number, x2: number, y2: number) {
    this.p1 = new Point(x1, y1)
    this.p2 = new Point(x2, y2)
  }

  isDiagonal(): boolean {
    return this.p1.x !== this.p2.x && this.p1.y !== this.p2.y
  }

  getPoints(): Array<Point> {
    if (this.isDiagonal()) {
      let [startX, stopX] = [this.p1.x, this.p2.x]
      let [startY, stopY] = [this.p1.y, this.p2.y]
      let xArr = Array.from({ length: Math.abs(stopX - startX ) + 1 }, (_, i) => {
        if (startX < stopX)
          return startX + i
        else
          return startX - i
      })
      let yArr = Array.from({ length: Math.abs(stopY - startY ) + 1 }, (_, i) => {
        if (startY < stopY)
          return startY + i
        else
          return startY - i
      })
      return xArr.map((x, i) => new Point(x, yArr[i]))
    } else if (this.p1.x === this.p2.x) {
      let [start, stop] = this.p1.y > this.p2.y ? [this.p2.y, this.p1.y] : [this.p1.y, this.p2.y]
      return Array.from({ length: Math.abs(stop - start) + 1 }, (_, i) => start + i).map(y => new Point(this.p1.x, y))
    } else { // y === y
      let [start, stop] = this.p1.x > this.p2.x ? [this.p2.x, this.p1.x] : [this.p1.x, this.p2.x]
      return Array.from({ length: (stop + 1 - start) }, (_, i) => start + i).map(x => new Point(x, this.p1.y))
    }
  }
}

const challengeCode = (data: String) => {
  let lines = parseData(data)
  let straightLines = lines.filter(line => !line.isDiagonal())
  console.log("Part 1")
  console.log(getIntersections(straightLines))
  console.log("Part 2")
  console.log(getIntersections(lines))
}

const getIntersections = (lines: Array<Line>): number => {
  let allPoints = lines.reduce((prev, curr) => {
    prev.push(...curr.getPoints())
    return prev
  }, new Array<Point>())
  let duplicates = allPoints.reduce((prev: Map<string, number>, curr) => {
    let pStr = JSON.stringify(curr)
    let count = (prev.get(pStr) || 0) + 1
    prev.set(pStr, count)
    return prev
  }, new Map<string, number>())
  // let intersections = allPoints.filter(p => duplicates.get(JSON.stringify(p)) || 0 >= 2)
  // console.log(duplicates)
  return Array.from(duplicates.values()).filter(point => point >= 2).length
}

const parseData = (data: String): Array<Line> => {
  return data.split('\n').map(el => {
    let [x, y] = el.split(' -> ')
    let [x1, y1] = x.split(',')
    let [x2, y2] = y.split(',')
    return new Line(parseInt(x1), parseInt(y1), parseInt(x2), parseInt(y2))
  })
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}