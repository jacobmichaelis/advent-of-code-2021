import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, '../dist/sample-input.txt')
let challengePath = path.join(__dirname, '../dist/input.txt')
let challengePath2 = path.join(__dirname, '../dist/input2.txt')

const challengeCode = (data: String) => {
  let positions = parseData(data)
  console.log(`Part 1 BF: ${alignCrabsBruteForce(positions)}`)
  console.log(`Part 1 Mid: ${alignCrabsToMiddle(positions)}`)
  console.log(`Part 2 BF: ${alignCrabsGaussBF(positions)}`)
}

const alignCrabsBruteForce = (positions: Array<number>): number => {
  let fuelList: Array<number> = []
  new Set(positions).forEach(pos => {
    fuelList.push(positions.reduce((prev, curr) => prev + Math.abs(curr - pos), 0))
  })
  return fuelList.sort((a, b) => a - b)[0]
}

const alignCrabsToMiddle = (positions: Array<number>): number => {
  let middle = positions.sort((a, b) => a - b)[positions.length/2]
  return positions.reduce((prev, curr) => prev + Math.abs(curr - middle), 0)
}

const alignCrabsGaussBF = (positions: Array<number>): number => {
  let fuelList: Array<number> = []
  let sorted = positions.sort((a, b) => a - b)
  let start = sorted[0]
  let stop = sorted[sorted.length - 1]

  ;[...Array(stop - start).keys()].map(x => x + start).forEach(x => {
    fuelList.push(positions.reduce((prev, curr) => prev + sumOfSeries(Math.abs(curr - x)), 0))
  })

  return fuelList.sort((a, b) => a - b)[0]
}

const sumOfSeries = (n: number): number => (n * (n + 1)) / 2

const parseData = (data: String): Array<number> => {
  return data.split(',').map(x => parseInt(x))
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}