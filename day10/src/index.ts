import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, '../dist/sample-input.txt')
let challengePath = path.join(__dirname, '../dist/input.txt')
let challengePath2 = path.join(__dirname, '../dist/input2.txt')

const errorScoreMap = new Map<string, number>([
  [')', 3],
  [']', 57],
  ['}', 1197],
  ['>', 25137],
])

const autoCompleteScoreMap = new Map<string, number>([
  [')', 1],
  [']', 2],
  ['}', 3],
  ['>', 4],
])

const chunkMap = new Map<string, string>([
  ['(', ')'],
  ['[', ']'],
  ['{', '}'],
  ['<', '>'],
])

const openChar = ['(', '[', '{', '<']
const isOpen = (c: string) => openChar.includes(c)
const closeChar = [')', ']', '}', '>']
const isClose = (c: string) => closeChar.includes(c)

const challengeCode = (data: string) => {
  let lines = parseData(data)
  let illegalChars = findIllegalCharacters(lines)
  let score = (illegalChars.filter(x => x) as Array<string>).reduce((score, c) => score += errorScoreMap.get(c) || 0, 0)
  console.log(`Part 1: ${score}`)
  let incompleteLines = lines.filter((_, i) => !illegalChars[i])
  let completions = completeLines(incompleteLines)
  console.log(`Part 2: ${scoreCompletions(completions)}`)
}

const findIllegalCharacters = (lines: Array<string>): Array<string | undefined> => {
  return lines.reduce((prev, curr) => {
    let stack = new Array<string>()
    prev.push(Array.from(curr).find(c => {
      if (isOpen(c)) stack.push(c)

      if (isClose(c)) {
        if (chunkMap.get(stack[stack.length - 1]) === c)
          stack.pop()
        else {
          return c
        }
      }
    }))
    return prev
  }, new Array<string | undefined>())
}

const completeLines = (lines: Array<string>): Array<Array<string>> => {
  return lines.reduce((prev, curr) => {
    let stack = Array.from(curr).reduce((stack, c) => {
      if (isOpen(c)) stack.push(c)
      else stack.pop() // making a huge assumption here that these are incomplete lines not errored lines
      return stack
    }, new Array<string>())
    prev.push(stack.reverse().map(x => chunkMap.get(x) || ''))
    return prev
  }, new Array<Array<string>>())
}

const scoreCompletions = (completions: Array<Array<string>>): number => {
  let scores = completions.map(completion => {
    return completion.reduce((score, c) => {
      score *= 5
      score += autoCompleteScoreMap.get(c) || 0
      return score
    }, 0)
  })
  return scores.sort((a, b) => a - b)[Math.round(scores.length/2) - 1]
}

const parseData = (data: string): Array<string> => {
  return data.split('\n')
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}