import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, "../dist/sample-input.txt")
let challengePath = path.join(__dirname, "../dist/input.txt")
let challengePath2 = path.join(__dirname, "../dist/input2.txt")

const BOARD_WIDTH = 5
const BOARD_INDEX = [...Array(BOARD_WIDTH).keys()]

const challengeCode = (data: String) => {
  let [numbers, boards] = parseData(data)
  console.log("Part 1")
  console.log(playBingo(numbers, JSON.parse(JSON.stringify(boards))))
  console.log("Part 2")
  console.log(loseBingo(numbers, JSON.parse(JSON.stringify(boards))))
}

const playBingo = (numbers: Array<string>, boards: Array<Array<string>>): number => {
  for (let n = 0; n < numbers.length; n++) {
    let number: string = numbers[n]
    for (let i = 0; i < boards.length; i++) {
      let board: Array<string> = boards[i]
      for (let j = 0; j < board.length; j++) {
        let square: string = boards[i][j]
        if (square === number) {
          boards[i][j] = `X${square}`
          // check x
          let column = j%BOARD_WIDTH
          let columnValues = BOARD_INDEX.map(x => board[x*BOARD_WIDTH + column])
          let checkX: boolean = columnValues.every(x => x.includes('X'))
          if (checkX) return returnBingo(boards[i], number)
          // check y
          let row = Math.floor(j/5)
          let rowValues = BOARD_INDEX.map(x => board[row*BOARD_WIDTH + x])
          let checkY: boolean = rowValues.every(x => x.includes('X'))
          if (checkY) return returnBingo(boards[i], number)
          break
        }
      }
    }
  }
  return -1
}

const loseBingo = (numbers: Array<string>, boards: Array<Array<string>>): number => {
  let checkBoards: Array<boolean> = Array.from({ length: boards.length }, (v, i) => false)
  for (let n = 0; n < numbers.length; n++) {
    let number: string = numbers[n]
    for (let i = 0; i < boards.length; i++) {
      let board: Array<string> = boards[i]
      if (checkBoards[i]) continue
      for (let j = 0; j < board.length; j++) {
        let square: string = boards[i][j]
        if (square === number) {
          boards[i][j] = `X${square}`
          // check x
          let column = j%BOARD_WIDTH
          let columnValues = BOARD_INDEX.map(x => board[x*BOARD_WIDTH + column])
          let checkX: boolean = columnValues.every(x => x.includes('X'))
          if (checkX) {
            checkBoards[i] = true
            if (checkBoards.filter(x => !x).length === 0) return returnBingo(boards[i], number)
          }
          // check y
          let row = Math.floor(j/5)
          let rowValues = BOARD_INDEX.map(x => board[row*BOARD_WIDTH + x])
          let checkY: boolean = rowValues.every(x => x.includes('X'))
          if (checkY) {
            checkBoards[i] = true
            if (checkBoards.filter(x => !x).length === 0) return returnBingo(boards[i], number)
          }
          break
        }
      }
    }
  }
  return -1
}

const returnBingo = (board: Array<string>, number: string): number => {
  let boardSum = board.reduce((prev, curr) => prev += parseInt(curr.includes('X') ? '0' : curr), 0)
  return boardSum * parseInt(number)
}

const parseData = (data: String): [Array<string>, Array<Array<string>>] => {
  let lines = data.split('\n')
  let numbers = lines[0].split(',')
  let tempBoard: Array<string> = []
  let boards = lines.slice(2).reduce((prev: Array<Array<string>>, curr) => {
    if (curr === '') {
      prev.push(tempBoard)
      tempBoard = []
      return prev
    }
    tempBoard.push(...curr.trim().split(/\s+/))
    return prev
  }, [])
  return [numbers, boards]
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}