import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, '../dist/sample-input.txt')
let challengePath = path.join(__dirname, '../dist/input.txt')
let challengePath2 = path.join(__dirname, '../dist/input2.txt')

const challengeCode = (data: String) => {
  console.log('Solution 1')
  console.log(solveGammaEpsilon(parseData(data)))
  console.log('Solution 2')
  console.log(oxygenCO2Sensor(parseData(data)))
}

const gammaEpsilon = (list: Array<string>): [string, string] => {
  let gamma = ''
  let epsilon = ''
  let itemLen = list[0].length
  let listLen = list.length
  for (let i = 0; i < itemLen; i++) {
    let indexCount: number = list.reduce((prev, curr) => prev += +curr[i], 0)
    let condition = indexCount >= listLen / 2
    gamma += condition ? '1' : '0'
    epsilon += condition ? '0' : '1'
  }
  // console.log(gamma)
  // console.log(epsilon)
  return [gamma, epsilon]
}

const solveGammaEpsilon = (list: Array<string>): number => {
  let [gamma, epsilon] = gammaEpsilon(list)
  return parseInt(gamma, 2) * parseInt(epsilon, 2)
}

const oxygenCO2Sensor = (list: Array<string>): number => {
  let oxygen: string | null = null
  let co2: string | null = null
  let itemLen = list[0].length
  let oxygenList = list
  let co2List = list
  for (let i = 0; i < itemLen; i++) {
    if (oxygen && co2) break
    if (!oxygen) {
      let [gamma, ] = gammaEpsilon(oxygenList)
      let tempList = oxygenList.filter(x => x[i] === gamma[i])
      if (tempList.length === 1)
        oxygen = tempList[0]
      else if (tempList.length === 0)
        oxygen = oxygenList[oxygenList.length - 1]
      else
        oxygenList = tempList
    }
    if (!co2) {
      let [, epsilon] = gammaEpsilon(co2List)
      let tempList = co2List.filter(x => x[i] === epsilon[i])
      if (tempList.length === 1)
        co2 = tempList[0]
      else if (tempList.length === 0)
        co2 = co2List[co2List.length - 1]
      else
        co2List = tempList
    }
  }
  // console.log(oxygen)
  // console.log(co2)
  if (!oxygen || !co2) return -1
  return parseInt(oxygen, 2) * parseInt(co2, 2)
}

const parseData = (data: String): Array<string> => {
  return data.split('\n')
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}