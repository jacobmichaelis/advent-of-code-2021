import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, "../dist/sample-input.txt")
let challengePath = path.join(__dirname, "../dist/input.txt")
let challengePath2 = path.join(__dirname, "../dist/input2.txt")
let mckay = path.join(__dirname, '../dist/mckay.txt')

fs.readFile(samplePath, 'utf-8', (error, data) => {
  if (error) {
    console.error(error)
    return
  }

  let list = data.split('\n').map(x => parseInt(x))

  console.log("Sample answer part 1:")
  console.log(countDepthIncreases(list))
  console.log("Sample answer part 2:")
  console.log(countSlidingWindowIncreases(list))
})

fs.readFile(challengePath, 'utf-8', (error, data) => {
  if (error) {
    console.error(error)
    return
  }

  let list = data.split('\n').map(x => parseInt(x))

  console.log("Challenge answer part 1:")
  console.log(countDepthIncreases(list))
  console.log("Challenge answer part 2:")
  console.log(countSlidingWindowIncreases(list))
})

fs.readFile(mckay, 'utf-8', (error, data) => {
  if (error) {
    console.error(error)
    return
  }

  let list = data.split('\n').map(x => parseInt(x))

  console.log("McKay answer part 1:")
  console.log(countDepthIncreases(list))
  console.log("McKay answer part 2:")
  console.log(countSlidingWindowIncreases(list))
})

const countDepthIncreases = (list: Array<number>) => {
  let count = 0
  let prevDepth: number
  list.forEach(depth => {
    if (prevDepth && prevDepth < depth) count++
    prevDepth = depth
  })
  return count
}

const countSlidingWindowIncreases = (list: Array<number>) => {
  let count = 0
  let prevDepths: Array<number> = []
  for (let i = 0; i < list.length - 2; i++) {
    let currDepths = [ list[i], list[i+1], list[i+2] ]
    if (prevDepths.length === 3 && sumOf(prevDepths) < sumOf(currDepths)) count++
    prevDepths = currDepths
  }
  return count
}

const sumOf = (list: Array<number>): number => list.reduce((prev, curr) => prev + curr, 0)