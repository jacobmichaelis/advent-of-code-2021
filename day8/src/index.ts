import * as fs from 'fs'
import * as path from 'path'

let samplePath = path.join(__dirname, '../dist/sample-input.txt')
let challengePath = path.join(__dirname, '../dist/input.txt')
let challengePath2 = path.join(__dirname, '../dist/input2.txt')

const challengeCode = (data: String) => {
  let inputs = parseInputs(data)
  let outputs = parseOutputs(data)
  console.log(`Part 1: ${findUniqueOutputs(outputs)}`)
  console.log(`Part 2: ${findAllEncodedOutputs(inputs, outputs)}`)
}

const findUniqueOutputs = (outputs: Array<Array<string>>) => {
  return outputs.reduce((prev, curr) => prev + curr.filter(x => x.length === 2 || x.length === 3 || x.length === 4 || x.length === 7).length, 0)
}

const findAllEncodedOutputs = (inputs: Array<Array<string>>, outputs: Array<Array<string>>): number => {
  return outputs.reduce((sum, output, i) => {
    let mapping = new Map<string, string>()
    let code1 = inputs[i].find(x => x.length === 2) || ''
    let code4 = inputs[i].find(x => x.length === 4) || ''
    let code7 = inputs[i].find(x => x.length === 3) || ''
    let code8 = inputs[i].find(x => x.length === 7) || ''
    let code6 = inputs[i].find(x => x.length === 6 && !Array.from(code1).every(y => Array.from(x).includes(y))) || ''
    let code9 = inputs[i].find(x => x.length === 6 && Array.from(code4).every(y => Array.from(x).includes(y))) || ''
    let code0 = inputs[i].find(x => x.length === 6 && x !== code6 && x !== code9) || ''
    let code5 = inputs[i].find(x => x.length === 5 && Array.from(x).every(y => Array.from(code6).includes(y))) || ''
    let code3 = inputs[i].find(x => x.length === 5 && Array.from(code1).every(y => Array.from(x).includes(y))) || ''
    let code2 = inputs[i].find(x => x.length === 5 && x !== code5 && x !== code3) || ''
    mapping.set(Array.from(code0).sort().join(), '0')
    mapping.set(Array.from(code1).sort().join(), '1')
    mapping.set(Array.from(code2).sort().join(), '2')
    mapping.set(Array.from(code3).sort().join(), '3')
    mapping.set(Array.from(code4).sort().join(), '4')
    mapping.set(Array.from(code5).sort().join(), '5')
    mapping.set(Array.from(code6).sort().join(), '6')
    mapping.set(Array.from(code7).sort().join(), '7')
    mapping.set(Array.from(code8).sort().join(), '8')
    mapping.set(Array.from(code9).sort().join(), '9')
    let numStr = output.reduce((num, x) => num += mapping.get(Array.from(x).sort().join()), '')
    return sum + parseInt(numStr)
  }, 0)
}

const parseInputs = (data: String): Array<Array<string>> => {
  if (!data) return []
  return data.split('\n').map(d => d.split(' | ')[0]).reduce((prev, curr) => {
    prev.push(curr.split(' '))
    return prev
  }, new Array<Array<string>>())
}

const parseOutputs = (data: String): Array<Array<string>> => {
  if (!data) return []
  return data.split('\n').map(d => d.split(' | ')[1]).reduce((prev, curr) => {
    prev.push(curr.split(' '))
    return prev
  }, new Array<Array<string>>())
}

try {
  console.log('Sample Input')
  challengeCode(fs.readFileSync(samplePath, 'utf-8'))
  console.log('Challenge Input 1')
  challengeCode(fs.readFileSync(challengePath, 'utf-8'))
  console.log('Challenge Input 2')
  challengeCode(fs.readFileSync(challengePath2, 'utf-8'))
} catch (err) {
  console.error(err)
}